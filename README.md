This is a remake/translation of a game called ["The Northnorth
Passage"](http://ifdb.tads.org/viewgame?id=a7ysdefal48plt1z) by Snowball Ice.

The content is © Caleb Wilson.
Translated by Enola and Oreolek.
Coded by Oreolek.

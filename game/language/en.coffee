module.exports = {
  restart: "Restart"
  north1: """
    You stand up, and Gillivray's mouth hangs open. "Furthermore--" he says again, specks of white gathering around his livid lips, and then you walk out the back door before hearing further what more he has to say. Leaving behind a shocked silence you walk north along a nameless alley and out into the bustling center of Boroughbury.

    ### The Corner of Mill and Main

    Endless buildings, stone in the lower stories and hewn wood in the upper, huddle as if for warmth in blocks. The streets are bumper to bumper with motor vehicles, the sidewalks elbow to kidney with pedestrians. Wide and roaring Main Street runs southwest to northeast, while the smaller Mill Street runs north and south.
  """
  north2: """
    You surge across Main barely waiting for a break in the traffic. A chemical wind from an auto rushing past inches behind you slaps you on the back. You walk north for three-quarters of an hour, until you come to a familiar address, 814 North Mill, as evening gathers strength.

    ### Your House, Garden

    It's small, but neatly groomed, a miniature in gold and emerald. Through the front window you can see the warm light of your living room, where you spot your wife, Alice, reading a book of fairytales to Arnold Jr. They look happy. You remember the book from your own childhood, when your father used it as a tool to help explain the idea of a "family curse." The smell of the book, leather and pipe smoke, still reminds you of him. They look so happy.

    Your front door is east. Mill Street turns here, running south and west. A tangled thicket is to the north.
  """
  north3: """
    You plunge into the thicket. Stems scrape, wet leaves slap you. When you can see the sky again it is very dark. Your shoes are ruined from the mud. How are there so many ruts here, in this scrubland? The lights of Boroughbury have faded behind you. You cross an abandoned lot, then a sheep field, then a trickle of a stream, then an empty field.

    ### Countryside

    The glacier-scarred land north of Boroughbury. Night. You are cold, hungry. This is nothing, though, really. The sky is smeared with clouds, dots of stars like specks of ice. You stand on a broad and shallow rural road, flowers between the wheeltracks, bordered by dense hedgerows. The road runs east and west.
      """
  north4: """
    You force your way through a hedge. Thorns score your face, make runs in your clothing. You walk north all through the night, feeling lighter and lighter from hunger, colder and damper from the settling dew. Towards morning you have come to the southern edge of a large city. The buildings resemble hives of glazed pottery. This must be Konigsburg, which you have never before found the time to visit.

    ### Konigsburg, Festival Day!

    There's a parade, and your northerly route aligns you with it. Cheering citizens throw paper roses from balconies high above you. The air is dense with the various smells of humanity. You are walking within a marching band, a thousand musicians in uniforms of blue, red, and white, with gold trim, and towering green feather plumes. It's hard to think inside this sawtoothed thunder of serpents, sackbuts, shawms, and a hundred species of drum.

    The parade is heading north along the main road towards the center of the city.
  """
  north5: """
    Marching north between the ranks of musicians you overtake them, and find yourself amidst a regiment of soldiers. The uniforms are similar, but they are armed with weapons of murder instead of aural assault.

    Then the parade turns to the east, cutting across the plaza that opens out before a great palace.

    You walk north through double doors, past a quartet of ruffed, bewigged guards bearing ornamental spears, down a hallway lined on either side with black-clad courtiers, and into a ballroom.

    ### A Ball

    The music is soft, yet echoes like chips of ice on the parquet floor. Is it a floor, or a field? At the edges of your sight are vast expanses of wallpaper, a vague impression of mauve and egg-yolk yellow. The ceiling is a vault full of crystal and candlelight. There are dozens of doors leading away in every direction from this place.
  """
  north6: """
    You cut north, through the heaviest knot of dancers. Plump, silky calves kick, floating hemlines like the dresses of mermaids swirl out of your way, their owners all glares and toothy smiles. Somewhere a violin bow drags squallingly across the strings. Someone shrieks.

    ### Throne Room

    Shaped like the inside of a beehive. Golden light comes through windows as tall and narrow as treetrunks. There's a whole forest of them, and of the pencil-thin, shining columns. At the north end of the room is a throne made of polished, greenish wood.

    The King stands half-up from the throne. His hand is raised. A man in a black suit lies dying beneath the throne. His white ruff is stained red with blood. A lady in oxblood velvet holds a pistol. Her mouth is open, halfway through shouting an anti-aristocratic slogan. Behind her, a group of guards has thrown off their Konigsburg colors and revealed the black and red of the revolution. They have knives, ancient hunting pieces. Loyal guards face them, rifles raised to their shoulders. The King's face has twisted masklike with horror.
  """
  north7: """
    You shove through the scene of the assassination attempt. Guns are fired, screams knit together into a tapestry of violence as old as all repressive regimes. Revolutionary forces pin your arms behind your back, and drag you down a dusty stone staircase. Rising behind you comes the whickering whirr of a mechanical guillotine. Your feet, as you are dragged underground to the north, trail blood which is not your own.

    ### Dungeon

    A low-ceilinged room, redolent of rats.
  """
  north8: """
    A stinking vertex, full of skulls.
  """
  north9: """
    A tight space, walls blotchy with mold, like warm frost.
  """
  north10: """
    A dripping cell. Please do not wonder what it is dripping with.
  """
  north11: """
    You emerge into dank afternoon light. Though having more the sheen of pewter, the sky seems silver after your time in the dungeon.

    ### Dockyard, With Refugees

    Hundreds of people, all crammed into a stone yard under the watchful gaze of soldiers of the new regime. It's probably unwise to call them revolutionaries anymore.

    To the north, a fat-hulled wooden ship sits low in the water. The refugees are waiting to board, and escape from this dreary place.
  """
  north12: """
    You cut sideways and through the lines, bulling your way onto the ship. Perhaps it is your slightly mad affect that makes people jump out of your way. Slightly? That's too kind.

    The ship, which is called the Morning Frost, sails north across the Bay of Dirgeholm, past an island haunted with frozen ghosts, and up the ice-choked Sickleglass River.

    Landing where the river becomes too rocky to sail, the refugees build a new town out of sprucewood and stacked sod.

    ### Wichfield

    The sky is dark green, and patterned like the ribs of a starving dog. Torches gleam behind windowflaps of hides scraped thin. Muffled laughter around you would seem to indicate that these people have found happiness at last. The main street, Pothole, runs east and west. A pub (The Last Gasp) is to the northeast. A small chapel, rich in candlelight, is to the northwest.
  """
  north13: () -> """
    The lights of Wichfield fade behind you. A midnight wind strips all the warmth from the air. Your feet are lumps of pickled ice.

    #{"north14".l()}
  """
  north14: """
    ### The Wastes

    The landscape varies little.
  """
  north15: () -> "north14".l()
  north16: () -> """
    #{"north15".l()}

    And any variations average each other out.
  """
  north17: () -> """
    #{"north16".l()}

    You feel light as a husk.
  """
  north18: () -> """
    #{"north17".l()}

    The air is clear as crystal.
  """
  north19: """
    ### The North Pole

    A crumpled landscape of bright snow and crisp shadow. There is nothing here. The sky is the deep blue of poison.

    The crystal tumor in your brain hums contentedly.
  """
  no_north: "There isn't any more north."
  north20: () -> """
    #{"no_north".l()}

    Snow drifts further up your body.
  """
  north21: () -> """
    #{"no_north".l()}

    Snow rises up around your ankles.
  """
  north22: () -> """
    #{"no_north".l()}

    You don't feel cold any more.
  """
  north23: () -> """
    #{"no_north".l()}

    Snow rises up around your waist.

The air around you cracks. The crack is in a novel direction; along it, just visible, a sunny glade, grass shining with dew like each blade is made of quartz and emerald.
  """
  north24: (turns) -> """
    #{"no_north".l()}

    Snow rises up over the top of your head.

    ### You have frozen

    <small>In that game you scored 0 out of a possible 1, in #{turns} turns.</small>
  """
  game_over: (turns) -> """
    The family curse, once activated, can neither be cured nor ignored.

    If you do not choose to go north then you shall die.

    Your soul gutters out.

    ### You have died

    <small>In that game you scored 0 out of a possible 1, in #{turns} turns.</small>
  """
  game_won: (turns) -> """
    You walk northnorth into the crack. The air is warm here, the air smells of flowers. A pleasant pocket. Across the glade, a man in a green frock coat and chestnut wig rises from a portable writing desk. His boots are shiny leather. He reaches a hand to you. His fingernails are long and wavy. His smile is very wide. There are fibers of meat between his yellowed teeth. His wig is crooked. His eyes are crooked, swinging left and right like they are mounted on oiled gimbles.

    "You made it!"

    He clasps your hand. His own is sweaty, crawling with parasites like transparent inchworms.

    "I've been waiting a century!"

    You open your mouth and nothing comes out.

    "You don't recognize me? I'm your great, great, great grandfather, Merridew Norton. All these years I have been waiting for a partner to help explore the Northnorth Passage... They said it did not exist, but look..."

    He gestures to the northnorth, where the land falls and climbs in haphazard terraces, strips of earth and mineral deposits laced together like shoddy wicker. The sky is pink, marred with smears of yellow like runny yolk.

    The speckled stars are brown.

    ### You have discovered a new direction

    <small>In that game you scored 1 out of a possible 1, in #{turns} turns.</small>
  """
  verb_north: [
    "north",
    "n",
    "go to north",
    "go north",
    "exit north",
  ]
  verb_northnorth: [
    "northnorth",
    "nn",
    "go to northnorth",
    "go northnorth",
    "exit northnorth",
  ]
  verb_gameover: [
    "south"
    "east"
    "west"
    "northwest"
    "northeast"
    "southeast"
    "southwest"
    "ne"
    "nw"
    "sw"
    "se"
    "e"
    "w"
    "s"
    "l"
    "look"
    "examine"
    "jump"
    "wield"
    "run"
    "stand"
    "back"
    "take"
    "get"
    "drop"
    "i"
    "inventory"
    "blow"
    "kill"
    "attack"
    "crawl"
    "steal"
    "swim"
    "climb"
    "up"
    "down"
    "lie"
    "scream"
    "yell"
    "shoot"
    "say"
    "whisper"
    "undo"
    "put"
    "eat"
    "in"
    "out"
    "inside"
    "outside"
    "read"
    "check"
    "wear"
    "sleep"
    "drink"
  ]
  verb_restart: [
    "restart"
  ]
  verb_help: [
    "help"
    "about"
  ]
  enter_command: "Enter a command"
  wrong_input: "That's not a verb I recognize."
  help: """
    **The Northnorth Passage** was written in 2014 by Snowball Ice (a.k.a. [Caleb Wilson](https://twitter.com/astrobolism)). You can email the author at *rotifer@gmail.com*

    Remade by [Oreolek](https://en.oreolek.ru) in 2016. The remake's source code is [on GitLab.](https://git.oreolek.ru/oreolek/northnorth) You can play the original game [here.](http://iplayif.com/?story=http%3A%2F%2Fwww.ifarchive.org%2Fif-archive%2Fgames%2Fzcode%2Fnorthnorth.z8)
  """
  undo: "Undo last turn"
}

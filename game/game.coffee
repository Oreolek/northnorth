require('salet')

i18n.push("ru", require('../../game/language/ru.coffee'))
i18n.push("en", require('../../game/language/en.coffee'))

salet.game_id = "7b9e659f-94bf-4b1b-9a2d-d46cbc8bc6c2"
salet.game_version = "1.1"
salet.autosave = true

$(document).ready(() ->
  salet.beginGame()
  salet.character.original_bg = $("body").css("background-color")
  $("#wronginput").hide()
  $("#wronginput").html("<p>"+"wrong_input".l()+"</p>")
  $("#help").hide()
  $("#help").html("<p>"+marked("help".l())+"</p>")
  $("#content").on("click", "#restart", (event) ->
    event.preventDefault()
    salet.eraseSave()
  )
  $("#content").on("click", "#undo", (event) ->
    event.preventDefault()
    salet.goBack()
  )
  $("#content").on("submit", "form", (event) ->
    event.preventDefault()
    has_action = false
    input = $(this).find("input[name=keyword]").val().toLowerCase()
    _paq.push(['setCustomDimension', 2, input]) # record the command
    list = salet.here().verbs()
    for index,verb of list
      for text in verb.verbs
        if (input == text)
          has_action = true
          verb.action()
    if has_action == false
      salet.view.showBlock("#wronginput")
    return false
  )
)

default_verbs = {
  north: {
    verbs: "verb_north".l()
    action: () ->
      choices = salet.getSituationIdChoices(salet.here().choices)
      salet.goTo(choices[0])
  },
  gameover: {
    verbs: "verb_gameover".l()
    action: () ->
      salet.goTo("game_over")
  }
  help: {
    verbs: "verb_help".l()
    action: () ->
      $("#help").show('slow', () ->
        salet.view.scrollToBottom()
      )
  }
  restart: {
    verbs: "verb_restart".l()
    action: () ->
      salet.eraseSave()
  }
}

croom = (title, options) ->
  options.clear ?= false
  options.canChoose ?= () ->
    return false
  options.canView ?= true
  options.priority ?= -2
  options.verbs ?= () ->
    return default_verbs
  options.optionText ?= "<form><input name='keyword' class='form-control command' type='text' placeholder='#{"enter_command".l()}'></input></form>"
  options.choices ?= "##{title}"
  # don't print the descriptions twice
  options.dsc ?= () ->
    if @visited > 1
      return ""
    return "#{title}".l()
  options.afterChoices ?= () ->
    if salet.interactive
      # Focus on the text input
      $('#current-room .command').trigger("focus")
      # we don't need slideUp effect here
      $("#wronginput").hide()
      $("#help").hide()
      # Scroll to the text input
      salet.view.scrollToBottom()
      # Piwik analytics: room stats
      _paq.push(['trackPageView', title])
      salet.character.turns++
    return true
  return room(title, options)

croom "start",
  dsc: ""
  ways: ["north1"]
  enter: () ->
    salet.character.turns = 0
  afterChoices: () -> # don't focus on the input
    _paq.push(['trackPageView', "start"])
    salet.character.turns++

croom "north1",
  tags: ["start"]
  ways: ["north2"]

# https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
# converted via js2coffee
shadeBlendConvert = (p, from, to) ->
  if typeof p != 'number' or p < -1 or p > 1 or typeof from != 'string' or from[0] != 'r' and from[0] != '#' or typeof to != 'string' and typeof to != 'undefined'
    return null
  if !@sbcRip

    @sbcRip = (d) ->
      l = d.length
      RGB = new Object
      if l > 9
        d = d.split(',')
        if d.length < 3 or d.length > 4
          return null
        RGB[0] = i(d[0].slice(4))
        RGB[1] = i(d[1])
        RGB[2] = i(d[2])
        RGB[3] = if d[3] then parseFloat(d[3]) else -1
      else
        if l == 8 or l == 6 or l < 4
          return null
        if l < 6
          d = '#' + d[1] + d[1] + d[2] + d[2] + d[3] + d[3] + (if l > 4 then d[4] + '' + d[4] else '')
        d = i(d.slice(1), 16)
        RGB[0] = d >> 16 & 255
        RGB[1] = d >> 8 & 255
        RGB[2] = d & 255
        RGB[3] = if l == 9 or l == 5 then r((d >> 24 & 255) / 255 * 10000) / 10000 else -1
      RGB

  i = parseInt
  r = Math.round
  h = from.length > 9
  h = if typeof to == 'string' then (if to.length > 9 then true else if to == 'c' then !h else false) else h
  b = p < 0
  p = if b then p * -1 else p
  to = if to and to != 'c' then to else if b then '#000000' else '#FFFFFF'
  f = sbcRip(from)
  t = sbcRip(to)
  if !f or !t
    return null
  if h
    'rgb(' + r((t[0] - (f[0])) * p + f[0]) + ',' + r((t[1] - (f[1])) * p + f[1]) + ',' + r((t[2] - (f[2])) * p + f[2]) + (if f[3] < 0 and t[3] < 0 then ')' else ',' + (if f[3] > -1 and t[3] > -1 then r(((t[3] - (f[3])) * p + f[3]) * 10000) / 10000 else if t[3] < 0 then f[3] else t[3]) + ')')
  else
    '#' + (0x100000000 + (if f[3] > -1 and t[3] > -1 then r(((t[3] - (f[3])) * p + f[3]) * 255) else if t[3] > -1 then r(t[3] * 255) else if f[3] > -1 then r(f[3] * 255) else 255) * 0x1000000 + r((t[0] - (f[0])) * p + f[0]) * 0x10000 + r((t[1] - (f[1])) * p + f[1]) * 0x100 + r((t[2] - (f[2])) * p + f[2])).toString(16).slice(if f[3] > -1 or t[3] > -1 then 1 else 3)

for i in [2..22]
  croom "north#{i}",
    tags: ["north#{i-1}"]
    ways: ["north#{i+1}"]
    index: i
    after: () -> # slowly make the background whitier
      new_color = shadeBlendConvert(@index*0.0416, salet.character.original_bg)
      # 0.0416 * 24 is approx. 1.0
      # if new_color is null then don't change it
      if new_color
        $("body").css("background-color", new_color)
      return ""

croom "north23",
  tags: ["north22"]
  ways: ["north24"]
  verbs: () ->
    verbs = default_verbs
    verbs.northnorth = {
      verbs: "verb_northnorth".l()
      action: () ->
        salet.goTo("game_won")
    }
    return verbs

restart_undo = """
  <div class="transient">
    <button id='restart' class='btn btn-outline-danger'>#{"restart".l()}</button>
    <button id='undo' class='btn btn-outline-primary'>#{"undo".l()}</button>
  </div>
"""

croom "north24",
  canSave: false
  tags: ["north23"]
  after: () ->
    $("#content").append(restart_undo)
    return ""
  dsc: () ->
    "north24".l(salet.character.turns)

croom "game_over",
  canSave: false
  dsc: () ->
    '<div class="transient">'+marked("game_over".l(salet.character.turns))+'</div>'
  after: () ->
  after: () ->
    $("#content").append(restart_undo)
    return ""

croom "game_won",
  canSave: false
  dsc: () ->
    "game_won".l(salet.character.turns)
